package com.hotmail.stripe103.pex.rankprestige;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainClass extends JavaPlugin implements Listener
{
    String PREFIX = ChatColor.GREEN + "[" + ChatColor.WHITE + "Prestige" + ChatColor.GREEN + "] ";

    // Settings variables
    String maxRank, minRank;
    boolean congratulate;
    String suffixStyle;
    boolean showNoPrestige;
    int prestigeCap;
    boolean maxPrestigeReset;

    File configFile;
    YamlConfiguration config;

    Logger log;

    @Override
    public void onEnable()
    {
        getServer().getPluginManager().registerEvents(this, this);
        this.log = getLogger();

        // Load user configuration
        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists())
        {
            configFile.getParentFile().mkdirs();
            copy(getResource("config.yml"), configFile);
        }
        if (configFile.exists())
            config = YamlConfiguration.loadConfiguration(configFile);

        this.maxRank = config.getString("maxRank");
        this.minRank = config.getString("minRank");
        this.congratulate = config.getBoolean("congratulate");

        this.suffixStyle = config.getString("suffixStyle");
        this.showNoPrestige = config.getBoolean("showNoPrestige");

        this.prestigeCap = config.getInt("prestigeCap");
        this.maxPrestigeReset = config.getBoolean("maxPrestigeReset");

        // Update the suffix of each player online in case the suffix style have been changed.
        for (Player p : getServer().getOnlinePlayers())
            updateSuffix(p);
    }

    private void logToFile(String message)
    {
        try
        {
            File dataFolder = getDataFolder();
            if (!dataFolder.exists())
                dataFolder.mkdirs();

            File saveTo = new File(getDataFolder(), "prestigelog.txt");
            if (!saveTo.exists())
                saveTo.createNewFile();

            FileWriter fw = new FileWriter(saveTo, true);
            PrintWriter pw = new PrintWriter(fw);

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();

            pw.println("[" + dateFormat.format(date) + "] " + message);
            pw.flush();

            pw.close();
            fw.close();
        }
        catch (IOException e)
        {
            this.log.log(Level.WARNING, "Could not log to plugin log file.");
        }
    }

    private void copy(InputStream in, File file)
    {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len=in.read(buf))>0)
            {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e)
    {
        updateSuffix(e.getPlayer());
    }

    private void updateSuffix(Player p)
    {
        // Add the 0 suffix if the player does not have one and update it for those that has.
        PermissionUser user = ru.tehkode.permissions.bukkit.PermissionsEx.getUser(p);

        int prestige = 0;
        try
        {
            prestige = Integer.parseInt(user.getOption("player-prestige"));
            if (prestige != 0)
            {
                HashMap<String, String> v = new HashMap<String, String>();
                v.put("p", "" + prestige);
                user.setSuffix(MessageUtil.replaceColors(MessageUtil.replaceVars(this.suffixStyle, v)), null);
                user.save();
            }
        }
        catch (Exception ex)
        {
            if (showNoPrestige)
            {
                logToFile("Player " + p.getName() + " logged in with no previous prestige level. Setting to 0.");
                HashMap<String, String> v = new HashMap<String, String>();
                v.put("p", "" + 0);
                user.setSuffix(MessageUtil.replaceColors(MessageUtil.replaceVars(this.suffixStyle, v)), null);
                user.save();
            }

        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String name, String[] args)
    {
        if (cmd.getName().equals("prestige"))
        {
            if (sender instanceof ConsoleCommandSender)
            {
                if (args.length != 1)
                { sender.sendMessage("Usage: /prestige <player>"); return true; }

                Player pPlayer = getServer().getPlayer(args[0]);

                if (pPlayer == null)
                {
                    sender.sendMessage(PREFIX + ChatColor.GOLD + "Player is not online.");
                    return true;
                }

                if (pPlayer.isOnline())
                {
                    if (!doPrestige(sender, pPlayer))
                        sender.sendMessage(PREFIX + ChatColor.GOLD + "Player could not be prestiged. Prerequisites not met.");
                    else
                        logToFile("Console prestiged player " + pPlayer.getName() + ". Level is now " + ru.tehkode.permissions.bukkit.PermissionsEx.getUser(pPlayer).getOption("player-prestige"));
                }
                else
                    sender.sendMessage(PREFIX + ChatColor.GOLD + "Player must be online to be prestiged.");
            }
            else if (sender instanceof Player)
            {
                Player p = (Player) sender;

                if (args.length == 1)
                {
                    if (p.isOp() || p.hasPermission("prestigeplayer.other"))
                    {
                        Player pPlayer = getServer().getPlayer(args[0]);

                        if (pPlayer == null)
                        {
                            sender.sendMessage(PREFIX + ChatColor.GOLD + "Player is not online.");
                            return true;
                        }

                        if (pPlayer.isOnline())
                        {
                            if (!doPrestige(sender, pPlayer))
                                sender.sendMessage(PREFIX + ChatColor.GOLD + "Player could not be prestiged. Prerequisites not met.");
                            else
                                logToFile("Player " + p.getName() + " force-prestiged " + pPlayer.getName() + ". Level is now " + ru.tehkode.permissions.bukkit.PermissionsEx.getUser(pPlayer).getOption("player-prestige"));
                        }
                    }
                    else
                    {
                        sender.sendMessage(PREFIX + ChatColor.GOLD + "You cannot prestige another player.");
                    }
                }
                else if (args.length == 0)
                {
                    if (!doPrestige(sender, p))
                        sender.sendMessage(PREFIX + ChatColor.GOLD + "You cannot prestige. Prerequisites not met.");
                    else
                        logToFile("Player " + p.getName() + " prestiged themselves");
                }
                else
                {
                    if (p.isOp() || p.hasPermission("prestigeplayer.other"))
                        sender.sendMessage("Usage: /prestige [player]");
                    else
                        sender.sendMessage("Usage: /prestige");
                }
            }
        }
        else if (cmd.getName().equals("setprestige"))
        {
            if (sender instanceof ConsoleCommandSender)
            {
                if (args.length != 2)
                { sender.sendMessage("Usage: /setprestige <level> <player>"); return true; }

                Player pPlayer = getServer().getPlayer(args[1]);
                int i = Integer.parseInt(args[0]);
                if (pPlayer == null)
                {
                    sender.sendMessage(PREFIX + ChatColor.GOLD + "Player is not online.");
                    return true;
                }

                if (pPlayer.isOnline())
                {
                    if(!setPrestige(sender, pPlayer, i))
                        sender.sendMessage(PREFIX + ChatColor.GOLD + "Player prestige could not be set.");
                    else
                        logToFile("Console set player " + pPlayer.getName() + "'s prestige level to " + i + ".");
                }
                else
                    sender.sendMessage(PREFIX + ChatColor.GOLD + "Player is not online.");
            }
            else if (sender instanceof Player)
            {
                Player pPlayer = (Player) sender;

                if (args.length == 1)
                {
                    if (pPlayer.isOp() || pPlayer.hasPermission("prestigeplayer.set"))
                    {
                        try
                        {
                            int i = Integer.parseInt(args[0]);
                            if (!setPrestige(sender, pPlayer, i))
                                sender.sendMessage(PREFIX + ChatColor.GOLD + "Player prestige could not be set.");
                            else
                                logToFile("Player " + pPlayer.getName() + " set their own prestige level to " + i + ".");
                            }
                        catch (NumberFormatException e)
                        {
                            sender.sendMessage(PREFIX + ChatColor.GOLD + "Not a valid level number. Must be an integer.");
                        }
                    }
                    else
                    {
                        sender.sendMessage(PREFIX + ChatColor.GOLD + "You do not have permission to set your prestige level.");
                    }
                }
                else if (args.length == 2)
                {
                    if (pPlayer.isOp() || pPlayer.hasPermission("playerprestige.set.other"))
                    {
                        try
                        {
                            int i = Integer.parseInt(args[0]);
                            if (!setPrestige(sender, getServer().getPlayer(args[1]), i))
                                sender.sendMessage(PREFIX + ChatColor.GOLD + "Player prestige could not be set.");
                            else
                                logToFile("Player " + sender.getName() + " set " + getServer().getPlayer(args[1]).getName() + "'s prestige level to " + i + ".");
                        }
                        catch (NumberFormatException e)
                        {
                            sender.sendMessage(PREFIX + ChatColor.GOLD + "Not a valid level number. Must be an integer.");
                        }
                    }
                    else
                    {
                        sender.sendMessage(PREFIX + ChatColor.GOLD + "You do not have permission to set another player's prestige level.");
                    }
                }
            }
        }

        return true;
    }

    private boolean setPrestige(CommandSender sender, Player p, int level)
    {
        PermissionUser user = ru.tehkode.permissions.bukkit.PermissionsEx.getUser(p);
        if (level != 0)
            user.setOption("player-prestige", "" + level);
        else
            user.setOption("player-prestige", null);

        HashMap<String, String> v = new HashMap<String, String>();
        v.put("p", "" + level);
        user.setSuffix(MessageUtil.replaceColors(MessageUtil.replaceVars(this.suffixStyle, v)), null);

        user.save();

        if (sender instanceof Player)
        {
            if (p.equals((Player) sender))
                sender.sendMessage(PREFIX + ChatColor.GREEN + "You have sucessfully changed your prestige level.");
            else
                sender.sendMessage(PREFIX + ChatColor.GREEN + "You have sucessfully changed the prestige level of " + p.getName());
        }
        else
            sender.sendMessage(PREFIX + ChatColor.GREEN + "You have sucessfully changed the prestige level of " + p.getName());

        return true;
    }

    private boolean doPrestige(CommandSender sender, Player p)
    {
        PermissionUser user = ru.tehkode.permissions.bukkit.PermissionsEx.getUser(p);
        PermissionGroup[] groups = user.getGroups();

        for (PermissionGroup g : groups)
        {
            if (g.getName().equals(this.maxRank))
            {
                String prestigeString = user.getOption("player-prestige");
                int prestige = 0;
                try
                {
                    if (prestigeString != "" || prestigeString != null)
                        prestige = Integer.parseInt(prestigeString);
                }
                catch (Exception e)
                {
                }

                if (prestige >= this.prestigeCap)
                {
                    sender.sendMessage(PREFIX + ChatColor.GOLD + "You have already reached the max prestige level.");
                    if (!maxPrestigeReset)
                        return true;
                }

                prestige++;

                logToFile("Removing user from group " + this.maxRank + " and adding to group " + this.minRank);
                user.removeGroup(this.maxRank);
                user.addGroup(this.minRank);

                HashMap<String, String> v = new HashMap<String, String>();
                v.put("p", "" + prestige);

                user.setSuffix(MessageUtil.replaceColors(MessageUtil.replaceVars(this.suffixStyle, v)), null);

                user.setOption("player-prestige", "" + prestige);
                user.save();

                // If the player has been prestiged by another player, send confirmation to the issuer.
                if (!sender.getName().equals(p.getName()))
                    sender.sendMessage(PREFIX + ChatColor.WHITE + " You have successfully prestiged " + p.getDisplayName() + ChatColor.WHITE + ".");

                p.sendMessage(ChatColor.GREEN + "=====================[ " + ChatColor.WHITE + "PRESTIGE" + ChatColor.GREEN + " ]=====================");
                p.sendMessage(ChatColor.WHITE + "You have been prestiged, resetting you back to the " + this.minRank + "-rank.");
                if (prestige >= this.prestigeCap)
                    p.sendMessage(ChatColor.WHITE + "However, since you are at the max prestige level already, you did not gain another level.");
                else
                {
                    if (prestige == 1)
                    {
                        p.sendMessage(ChatColor.GOLD + "Congratulations on your first prestige!");

                        if (this.congratulate)
                        {
                            for (Player player : getServer().getOnlinePlayers())
                            {
                                if (!player.equals(p))
                                {
                                    player.sendMessage(PREFIX + ChatColor.WHITE + "Congratulations to " + p.getDisplayName() +
                                    ChatColor.WHITE + " on their first prestige!");
                                }
                            }
                        }
                    }
                    else
                    {
                        p.sendMessage(ChatColor.GOLD + "Your prestige level is now " + prestige);
                    }
                }
                p.sendMessage(ChatColor.GREEN + "====================================================");

                return true;
            }
        }

        return false;
    }
}
