package com.hotmail.stripe103.pex.rankprestige;

import java.util.HashMap;

/**
 * Project: HomieServerMisc
 * User: Stripe103
 * Date: 2013-07-03
 * Time: 13:03
 */
public class MessageUtil
{
    public static String replaceColors(String s)
    {
        return s.replaceAll("(&([a-fk-or0-9]))", "§$2");
    }

    public static String replaceVars(String msg, HashMap<String, String> variables)
    {
        for (String s : variables.keySet())
        {
            try
            {
                msg = msg.replace("${" + s + "}", (CharSequence)variables.get(s));
            }
            catch (Exception e)
            {
                System.out.println("Failed to replace string vars. Error on " + s);
            }
        }

        return msg;
    }
}
