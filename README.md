Prestige Plugin
===============
*Author:* Stripe103

PermissionsEx must be installed for the plugin to work.

Basic player commands
---------------------
**/prestige**

*Permission Node:* prestigeplayer.use

Restart the rank progress and gain a prestige suffix level.

OP default commands
-------------------
**/prestige <player>**

*Permission Node:* prestigeplayer.other

Let's a player force another player to prestige. Only works if the player are in the level required.

**/setprestige <level>**

*Permission Node*: prestigeplayer.set

Sets the players own prestige level to the desired level. Negative levels also allowed if you want to use that.

**/setprestige <level> <player>**

*Permission Node*: prestigeplayer.set.other

Allows a player to set another player's prestige level to the desired level.